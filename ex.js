// BT1
function bt1() {
  for (a = 0, num = 1; a < 10000; num++) {
    a += num;
  }
  num = num - 1;
  document.getElementById(
    "bt1"
  ).innerText = `Số nguyên dương nhỏ nhất : ${num}`;
}
// BT2
function bt2() {
  // var x = document.getElementById("nhapSoX").value * 1;
  // var n = document.getElementById("nhapSoN").value * 1;
  for (
    var mu = 1,
      sum = 0,
      x = document.getElementById("nhapSoX").value * 1,
      n = document.getElementById("nhapSoN").value * 1;
    mu <= n;
    mu++
  ) {
    sum += Math.pow(x, mu);
  }
  document.getElementById("bt2").innerText = `Kết quả : ${sum}`;
}
// BT3
function bt3() {
  // var n = document.getElementById("nhapGiaiThua").value * 1;
  for (
    a = 1, sum = 1, n = document.getElementById("nhapGiaiThua").value * 1;
    a <= n;
    a++
  ) {
    sum = sum * a;
  }
  document.getElementById("bt3").innerText = `Giai Thừa : ${sum}`;
}
// BT4
function bt4() {
  for (i = 1, a = ""; i <= 10; i++) {
    if (i % 2 == 0) {
      a = a + "<div class='bg-danger text-white'>Div chẵn</div>";
    } else {
      a = a + "<div class='bg-primary text-white'>Div lẻ</div>";
    }
  }
  document.getElementById("bt4").innerHTML = a;
}
// BT5
function bt5() {
  var x = document.getElementById("nhapNguyenTo").value * 1;
  var a = "";
  function kiemTra(SNT) {
    var ketQua = true;
    for (var i = 2; i < SNT; i++) {
      if (SNT % i == 0) {
        ketQua = false;
        break;
      }
    }
    if (ketQua == true) {
      return SNT;
    } else {
      return "";
    }
  }
  for (i = 2; i <= x; i++) {
    a = a + " " + kiemTra(i);
  }

  document.getElementById("bt5").innerText = `Số nguyên tố : ${a}`;
}
